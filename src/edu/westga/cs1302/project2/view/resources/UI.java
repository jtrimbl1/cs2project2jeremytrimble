package edu.westga.cs1302.project2.view.resources;

/**
 * The Class UI defines output strings that are displayed for the user to see.
 * 
 * @author CS1302
 */
public final class UI {

	/**
	 * Defines string messages for exception messages for sky animation.
	 */
	public static final class ExceptionMessages {
		public static final String NULL_SKY = "sky cannot be null.";
		public static final String BAD_DIRECTION = "Invalid direction";

		public static final String X_GREATER_THAN_ZERO = "X must be greater than 0";
		public static final String Y_GREATER_THAN_ZERO = "Y must be greater than 0";
		public static final String WIDTH_GREATER_THAN_ZERO = "Width must be greater than 0";
		public static final String HEIGHT_GREATER_THAN_ZERO = "Height must be greater than 0";
		public static final String NULL_COLOR = "Color cannot be null";
		public static final String NULL_INSECT = "WingedInsect cannot be null";

		public static final String NULL_INSECT_COLLECTION = "Insect Collection cannot be null";
		public static final String CONTAINS_NULL_INSECT = "Insect Collection cannot contain null";
		public static final String SPEED_GREATER_THAN_ZERO = "Speed must be greater than 0";

		public static final String DIAMETER_GREATER_THAN_ZERO = "The sun's diameter must be greater than 0";
	}

}
