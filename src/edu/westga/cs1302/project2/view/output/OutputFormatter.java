package edu.westga.cs1302.project2.view.output;

import java.util.Collections;
import java.util.List;

import edu.westga.cs1302.project2.model.Butterfly;
import edu.westga.cs1302.project2.model.Dragonfly;
import edu.westga.cs1302.project2.model.Flying;
import edu.westga.cs1302.project2.model.Sky;
import edu.westga.cs1302.project2.model.WingedInsect;
import edu.westga.cs1302.project2.view.resources.UI;

/**
 * OutputFormatter builds summaries and reports for Skys.
 *
 * @author CS1302
 */
public class OutputFormatter {

	/**
	 * Initializes the OutputFormatter
	 */
	public OutputFormatter() {
	}

	/**
	 * Creates a report by analyzing a given sky.
	 * 
	 * @precondition sky != null
	 * @postcondition none
	 *
	 * @param sky
	 *            sky to analyze
	 * @return String with the given report
	 */
	public String buildReport(Sky sky) {
		if (sky == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_SKY);
		}
		List<WingedInsect> insectsOnly = sky.getInsects();

		String summary = "Sky Report:" + System.lineSeparator();
		summary += System.lineSeparator() + "Number Butterflies: " + sky.countOf(pred -> pred instanceof Butterfly);
		summary += System.lineSeparator() + "Number Dragonflies: " + sky.countOf(pred -> pred instanceof Dragonfly)
				+ System.lineSeparator();
		summary += System.lineSeparator() + "Smallest insect: " + Collections.min(insectsOnly);
		summary += System.lineSeparator() + "Largest insect: " + Collections.max(insectsOnly) + System.lineSeparator();
		summary += System.lineSeparator() + "Insects in sorted order by width" + System.lineSeparator();

		Collections.sort(insectsOnly);
		for (Flying insect : insectsOnly) {
			summary += insect + System.lineSeparator();
		}

		summary += System.lineSeparator();
		Collections.sort(insectsOnly, (insect1, insect2) -> {
			if (insect1.getClass().equals(insect2.getClass())) {
				return 0;
			} else if (insect1 instanceof Butterfly) {
				return -1;
			} else {
				return 1;
			}
		});
		summary += "Insects in sorted order by insect and then width" + System.lineSeparator();
		for (Flying insect : insectsOnly) {
			summary += insect + System.lineSeparator();
		}
		return summary;
	}
}
