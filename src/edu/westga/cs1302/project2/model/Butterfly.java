package edu.westga.cs1302.project2.model;

import edu.westga.cs1302.project2.view.resources.UI;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;

/**
 * The Butterfly Clas
 * 
 * @author jeremy.trimble
 * @version 10/20/2018
 */
public class Butterfly extends WingedInsect {

	private static final int SCALE_FACTOR = 100;
	private static final int FLYING_OFFSET = 10;
	private Color color;
	private boolean wingsUp;
	private GraphicsContext gc;

	/**
	 * Creates a new Butterfly
	 * 
	 * @precondition color != null
	 * @postcondition new Butterfly is created
	 * 
	 * @param x
	 *            the x value of the location of the Butterfly
	 * @param y
	 *            the y value of the location of the Butterfly
	 * @param size
	 *            the value of the width and height of the Butterfly
	 * @param color
	 *            the color of the Butterfly
	 * @param speed
	 *            the speed of the Butterfly
	 * 
	 */
	public Butterfly(double x, double y, double size, Color color, int speed) {
		super(x, y, size, size, speed);

		if (color == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_COLOR);
		}
		this.color = color;
		this.wingsUp = true;
		this.gc = this.getGraphicsContext2D();
		double xScale = this.getWidth() / SCALE_FACTOR;
		double yScale = this.getHeight() / SCALE_FACTOR;
		this.gc.scale(xScale, yScale);
	}

	@Override
	public void draw() {
		this.gc.clearRect(0, 0, SCALE_FACTOR, SCALE_FACTOR);
		this.drawBody();
		if (this.wingsUp) {
			this.setLayoutY(this.getLayoutY() + FLYING_OFFSET);
			this.drawWingsUp();
			this.wingsUp = false;
		} else if (!this.wingsUp) {
			this.drawWingsDown();
			this.setLayoutY(this.getLayoutY() - FLYING_OFFSET);
			this.wingsUp = true;
		}

	}

	private void drawWingsDown() {
		this.gc.setFill(this.color);
		this.gc.setStroke(this.color);

		double[] xPoints = { 10, 50, 90, 50 };
		double[] yPoints = { 95, 85, 90, 50 };
		this.gc.fillPolygon(xPoints, yPoints, 4);

	}

	@Override
	public String toString() {
		return "Butterfly width=" + this.getWidth() + " speed = " + this.getSpeed() + " color=" + this.color;
	}

	private void drawBody() {
		this.gc.setFill(Color.DARKGRAY);
		this.gc.setStroke(Color.DARKGRAY);
		this.gc.fillOval(20, 45, 60, 10);
		if (this.getDirection() == Direction.LEFT) {
			this.gc.strokeArc(10, 37, 20, 30, 0, 90, ArcType.OPEN);
		} else {
			this.gc.strokeArc(75, 35, 20, 30, 180, -90, ArcType.OPEN);
		}

	}

	private void drawWingsUp() {
		this.gc.setFill(this.color);
		this.gc.setStroke(this.color);

		double[] xPoints = { 10, 50, 90, 50 };
		double[] yPoints = { 5, 15, 10, 50 };
		this.gc.fillPolygon(xPoints, yPoints, 4);

	}

	/**
	 * Returns the color of the Butterfly object
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the color of the Butterfly
	 * @return
	 */
	public Color getColor() {
		return this.color;
	}

	@Override
	public void fly() {
		if (this.getLayoutY() + FLYING_OFFSET >= Sky.HEIGHT) {
			this.setLayoutY(this.getLayoutY() - FLYING_OFFSET);
		}
		if (this.wingsUp) {
			this.setLayoutY(this.getLayoutY() - FLYING_OFFSET);
		} else if (!this.wingsUp) {
			this.setLayoutY(this.getLayoutY() + FLYING_OFFSET);
		}
		super.fly();
		this.draw();

	}

	/**
	 * Returns the state of wings
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return true if wings are up, else false
	 */
	public boolean isWingsUp() {
		return this.wingsUp;
	}

	/**
	 * Sets whether wings are up or not
	 * 
	 * @precondition none
	 * @postcondition this.wingsUp == wingsUp
	 * 
	 * @param wingsUp
	 *            true if wings are to be set as up, else false
	 */
	public void setWingsUp(boolean wingsUp) {
		this.wingsUp = wingsUp;
	}

}
