package edu.westga.cs1302.project2.model.manager;

import java.util.Random;

import edu.westga.cs1302.project2.model.Butterfly;
import edu.westga.cs1302.project2.model.Dragonfly;
import edu.westga.cs1302.project2.model.Flying;
import edu.westga.cs1302.project2.model.Sky;
import edu.westga.cs1302.project2.model.SpottedButterfly;
import edu.westga.cs1302.project2.model.Sun;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

/**
 * The Class SkyManager.
 * 
 * @author CS1302
 */
public class SkyManager extends Pane {
	private Sky sky;
	private static final int MIN_SIZE = 50;
	private static final int MAX_SIZE = 100;
	private static final int NUM_OF_INSECTS = 8;
	private static final int MAX_SPEED = 40;
	private static final int MIN_SPEED = 15;

	/**
	 * Instantiates a new sky.
	 * 
	 * @precondition none
	 * @postcondition getSky() != null
	 */
	public SkyManager() {
		this.sky = new Sky();
		this.getChildren().add(this.sky);
	}

	/**
	 * Gets the sky.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the sky
	 */
	public Sky getSky() {
		return this.sky;
	}

	/**
	 * Sets the up bugs and the sun.
	 * 
	 * @precondition none
	 * @postcondition getSky().getInsects().getSize() == nInsects
	 */
	public void setupBugsAndSun() {
		this.generateInsects();
		this.createSun();
	}

	private void generateInsects() {
		Random rand = new Random();
		Color[] colors = { Color.BLUE, Color.YELLOW, Color.ORANGE, Color.DARKGREEN };
		Color[] spotColors = { Color.RED, Color.BLACK, Color.ANTIQUEWHITE, Color.GRAY, Color.BROWN, Color.FUCHSIA };

		for (int i = 0; i < NUM_OF_INSECTS; i++) {
			Color randColor = colors[rand.nextInt(colors.length)];
			int randSize = rand.nextInt((SkyManager.MAX_SIZE - SkyManager.MIN_SIZE)) + SkyManager.MIN_SIZE;
			int randX = rand.nextInt((Sky.WIDTH - randSize) + 1);
			int randY = rand.nextInt((Sky.HEIGHT - randSize) + 1);
			int randSpeed = rand.nextInt((SkyManager.MAX_SPEED - SkyManager.MIN_SPEED) + SkyManager.MIN_SIZE);
			double butterOrDragon = rand.nextDouble();
			Color spotColor = spotColors[rand.nextInt(6)];

			if (butterOrDragon < .25) {
				Dragonfly dragonfly = new Dragonfly(randX, randY, randSize, randSpeed);
				this.sky.addWingedInsect(dragonfly);
				this.getChildren().add(dragonfly);
				dragonfly.draw();
			} else if (butterOrDragon < .6) {
				Butterfly butterfly = new Butterfly(randX, randY, randSize, randColor, randSpeed);
				this.sky.addWingedInsect(butterfly);
				this.getChildren().add(butterfly);
			} else {
				SpottedButterfly spottedButterfly = new SpottedButterfly(randX, randY, randSize, randColor, randSpeed,
						spotColor);
				this.sky.addWingedInsect(spottedButterfly);
				this.getChildren().add(spottedButterfly);
			}
		}
	}

	private void createSun() {
		Sun sun = new Sun(100);
		this.sky.add(sun);
		this.getChildren().add(sun);
	}

	/**
	 * Move all flying objects in the sky
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public void update() {
		for (Flying insect : this.sky) {
			insect.fly();
		}
	}
}
