package edu.westga.cs1302.project2.model;

import edu.westga.cs1302.project2.view.resources.UI;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 * The Sun Class
 * 
 * @author jeremy.trimble
 * @version 10/25/2018
 */
public class Sun extends Canvas implements Flying {
	private double radius;
	public static final int RAY_OFFSET = 30;
	private int degreeInSky;

	/**
	 * Creates a new Sun Object
	 * 
	 * @precondition width > 0 && height > 0
	 * @postcondition new Sun is created.
	 * 
	 * @param diameter
	 *            the diameter of the Sun
	 */
	public Sun(double diameter) {
		super(diameter + Sun.RAY_OFFSET, diameter + Sun.RAY_OFFSET);
		if (diameter <= 0) {
			throw new IllegalArgumentException(UI.ExceptionMessages.DIAMETER_GREATER_THAN_ZERO);
		}

		this.radius = diameter / 2.0;
		this.setLayoutX(0);
		this.setLayoutY(Sky.HEIGHT - this.getHeight());
		this.degreeInSky = 180;
	}

	@Override
	public void fly() {
		GraphicsContext gc = this.getGraphicsContext2D();
		double skyRadius = (Sky.WIDTH / 2) - (this.getWidth() / 2);
		this.draw();

		double currentX = this.getLayoutX();
		double currentY = this.getLayoutY();
		this.degreeInSky = this.degreeInSky - 5;
		double theta = Math.toRadians(this.degreeInSky);
		double newX = skyRadius + (Math.cos(theta) * skyRadius) - (this.getWidth() / 2);
		double newY = skyRadius - (Math.sin(theta) * skyRadius);
		this.setLayoutX(newX);
		this.setLayoutY(newY);
		if (this.degreeInSky == 0) {
			this.degreeInSky = 180;
		}
	}

	/**
	 * Draws the sun
	 * 
	 * @precondition none
	 * @postcondition the sun is drawn
	 * 
	 */
	public void draw() {
		this.drawSunBody();
		this.drawRays();
	}

	private void drawSunBody() {
		GraphicsContext gc = this.getGraphicsContext2D();
		gc.setFill(Color.YELLOW);
		gc.fillOval(Sun.RAY_OFFSET / 2, Sun.RAY_OFFSET / 2, this.radius * 2, this.radius * 2);
	}

	private void drawRays() {
		GraphicsContext gc = this.getGraphicsContext2D();
		for (int i = 1; i <= 6; i++) {
			double angle = 30 * i;
			double anglePlus = angle + 180.0;
			double theta1 = Math.toRadians(angle);
			double theta2 = Math.toRadians(anglePlus);
			double rayLength = this.radius + (Sun.RAY_OFFSET / 2);
			double firstX = rayLength + (Math.cos(theta1) * rayLength);
			double firstY = rayLength - (Math.sin(theta1) * rayLength);
			double secondX = rayLength + (Math.cos(theta2) * rayLength);
			double secondY = rayLength - (Math.sin(theta2) * rayLength);

			gc.setStroke(Color.YELLOW);
			gc.strokeLine(firstX, firstY, secondX, secondY);
		}

	}

}
