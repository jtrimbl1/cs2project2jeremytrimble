package edu.westga.cs1302.project2.model;

import edu.westga.cs1302.project2.view.resources.UI;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;

/**
 * The WingedInsect class
 * 
 * @author jeremy.trimble
 * @version 10/20/2018
 *
 */
public abstract class WingedInsect extends Canvas implements Comparable<WingedInsect>, Flying {
	private Direction direction;
	private int speed;

	/**
	 * Creates a new WingedInsect
	 * 
	 * @precondition x > 0 && y > 0, && width > 0 && height > 0
	 * @postcondition new wingedinsect object exist
	 * @param x
	 *            the x value of the insect
	 * @param y
	 *            the y value of the insect
	 * @param width
	 *            the width of the insect
	 * @param height
	 *            the height of the insect
	 * @param speed
	 *            the speed of the insect
	 */
	public WingedInsect(double x, double y, double width, double height, int speed) {
		super(width, height);

		if (x < 0) {
			throw new IllegalArgumentException(UI.ExceptionMessages.X_GREATER_THAN_ZERO);
		}
		if (y < 0) {
			throw new IllegalArgumentException(UI.ExceptionMessages.Y_GREATER_THAN_ZERO);
		}
		if (width <= 0) {
			throw new IllegalArgumentException(UI.ExceptionMessages.WIDTH_GREATER_THAN_ZERO);
		}
		if (height <= 0) {
			throw new IllegalArgumentException(UI.ExceptionMessages.HEIGHT_GREATER_THAN_ZERO);
		}
		if (speed < 0) {
			throw new IllegalArgumentException(UI.ExceptionMessages.SPEED_GREATER_THAN_ZERO);
		}

		this.setLayoutX(x);
		this.setLayoutY(y);

		this.direction = Direction.getRandomDirection();
		this.speed = speed;

	}

	/**
	 * Clears the WingedInsect
	 * 
	 * @precondition none
	 * @postcondition WingedInsect graphics are removed
	 */
	public void clear() {
		GraphicsContext gc = this.getGraphicsContext2D();
		gc.clearRect(0, 0, this.getWidth(), this.getHeight());
	}

	/**
	 * Returns the direction of the WingedInsect
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the direction of the WingedInsect
	 */
	public Direction getDirection() {
		return this.direction;
	}

	/**
	 * Returns the speed of the WingedInsect
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the speed of the WingedInsect
	 */
	public int getSpeed() {
		return this.speed;
	}

	/**
	 * Draws the WingedInsect
	 * 
	 * @precondition none
	 * @postcondition WingedInsect is drawn
	 */
	public abstract void draw();

	@Override
	public int compareTo(WingedInsect other) {
		if (this.getWidth() > other.getWidth()) {
			return 1;
		} else if (this.getWidth() < other.getWidth()) {
			return -1;
		}
		return 0;
	}

	@Override
	public void fly() {
		if (this.getDirection() == Direction.RIGHT) {
			if (this.getLayoutX() + 150 >= Sky.WIDTH) {
				this.setLayoutX(0);
			} else {
				this.setLayoutX(this.getLayoutX() + this.speed);
			}

		}
		if (this.getDirection() == Direction.LEFT) {
			if (this.getLayoutX() <= 1 + this.speed) {
				this.setLayoutX(Sky.WIDTH - 100);
			}
			this.setLayoutX(this.getLayoutX() - this.speed);

		}

	}

}
