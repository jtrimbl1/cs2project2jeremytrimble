package edu.westga.cs1302.project2.model;

/**
 * The Flying Interface
 * 
 * @author jeremy.trimble
 * @version 10/23/2018
 */
public interface Flying {

	/**
	 * Makes the object fly
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 */
	void fly();
}
