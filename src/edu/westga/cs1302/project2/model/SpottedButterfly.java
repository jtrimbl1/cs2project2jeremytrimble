package edu.westga.cs1302.project2.model;

import edu.westga.cs1302.project2.view.resources.UI;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 * The SpottedButterfly Class
 * 
 * @author jeremy.trimble
 * @version 10/25/2018
 */
public class SpottedButterfly extends Butterfly {

	private Color spotColor;

	/**
	 * Creates a SpottedButterfly
	 * 
	 * @precondition wingColor != null
	 * @postcondition new SpottedButterfly is created
	 * 
	 * @param x
	 *            the x location of the SpottedButterfly
	 * @param y
	 *            the y location of the SpottedButterfly
	 * @param size
	 *            the size of the SpottedButterfly
	 * @param color
	 *            the color of the SpottedButterfly
	 * @param speed
	 *            the speed of the SpottedButterfly
	 * @param spotColor
	 *            the color of the SpottedButterfly's spots
	 */
	public SpottedButterfly(double x, double y, double size, Color color, int speed, Color spotColor) {
		super(x, y, size, color, speed);
		if (spotColor == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_COLOR);
		}

		this.spotColor = spotColor;

	}

	private void drawSpotsWingsUp() {
		GraphicsContext gc = this.getGraphicsContext2D();
		gc.setFill(this.spotColor);
		gc.fillOval(35, 20, 5, 5);
		gc.fillOval(47.5, 30, 5, 5);
		gc.fillOval(65, 20, 5, 5);
	}

	private void drawSpotsWingsDown() {
		GraphicsContext gc = this.getGraphicsContext2D();
		gc.setFill(this.spotColor);
		gc.fillOval(35, 75, 5, 5);
		gc.fillOval(47.5, 65, 5, 5);
		gc.fillOval(65, 75, 5, 5);
	}

	@Override
	public void draw() {
		super.draw();
		if (!this.isWingsUp()) {
			this.drawSpotsWingsUp();
		} else if (this.isWingsUp()) {
			this.drawSpotsWingsDown();
		}
	}

	@Override
	public String toString() {
		return "SpottedButterfly width=" + this.getWidth() + " speed = " + this.getSpeed() + " color="
				+ this.getColor();
	}

}
