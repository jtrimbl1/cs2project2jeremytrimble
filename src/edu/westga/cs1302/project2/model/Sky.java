package edu.westga.cs1302.project2.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Predicate;

import edu.westga.cs1302.project2.view.resources.UI;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

/**
 * The Class Sky.
 * 
 * @author CS1302
 */
public class Sky extends Rectangle implements Iterable<Flying> {

	public static final int WIDTH = 750;
	public static final int HEIGHT = 450;
	private List<Flying> insects;

	/**
	 * Instantiates a new sky.
	 * 
	 * @precondition none
	 * @postcondition insects.size() == 0 && clouds.size() == 0
	 */
	public Sky() {
		super(0, 0, Sky.WIDTH, Sky.HEIGHT);
		this.setFill(Color.CORNFLOWERBLUE);
		this.insects = new ArrayList<Flying>();
	}

	/**
	 * Returns the list of WingedInsects
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the list of WingedInsects
	 */
	public List<WingedInsect> getInsects() {
		List<WingedInsect> insectsOnly = new ArrayList<WingedInsect>();
		for (Flying insect : this.insects) {
			if (insect instanceof WingedInsect) {
				insectsOnly.add((WingedInsect) insect);
			}
		}

		return insectsOnly;
	}

	/**
	 * Adds the specified WingedInsect to the List of WingedInsects
	 * 
	 * @precondition insect != null
	 * @postcondition this.insects.size() = this.insects.size()@prev + 1
	 * 
	 * @param insect
	 *            the WingedInsect to be added
	 * 
	 * @return true if WingedInsect was add, else false
	 */
	public boolean addWingedInsect(WingedInsect insect) {
		if (insect == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_INSECT);
		}

		return this.insects.add(insect);
	}

	/**
	 * Adds the specified WingedInsect to the list.
	 * 
	 * @precondition insect != null
	 * @postcondition getSize() = getSize()@prev + 1
	 * @param insect
	 *            the WingedInsect to be added
	 * 
	 * @return true if the WingedInsect was added, else false
	 */
	public boolean add(Flying insect) {
		if (insect == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_INSECT);
		}
		return this.insects.add(insect);
	}

	/**
	 * Returns the count of predicate value.
	 * 
	 * @param pred
	 *            the predicate to validate
	 * @return the count of true predicates
	 */
	public int countOf(Predicate<Flying> pred) {
		int count = 0;
		for (Flying insect : this.insects) {
			if (pred.test(insect)) {
				count++;
			}
		}
		return count;
	}

	@Override
	public Iterator<Flying> iterator() {
		List<Flying> fliers = new ArrayList<Flying>();
		fliers.addAll(this.insects);
		return fliers.iterator();
	}

}
