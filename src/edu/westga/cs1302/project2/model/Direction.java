package edu.westga.cs1302.project2.model;

import java.util.Random;

import edu.westga.cs1302.project2.view.resources.UI;

/**
 * The enum Direction
 * 
 * @author jeremy.trimble
 *
 */
public enum Direction {
	LEFT, RIGHT;

	/**
	 * Parse the string to an enum
	 * 
	 * @precondition direction must be "Left" or "Right" case insensitive
	 * @postcondition none
	 * 
	 * @param direction
	 *            the direction to be parsed
	 * 
	 * @return the direction enum
	 */
	public Direction parseDirection(String direction) {
		if (direction.toLowerCase().equals("left")) {
			return LEFT;
		} else if (direction.toLowerCase().equals("right")) {
			return RIGHT;
		} else {
			throw new IllegalArgumentException(UI.ExceptionMessages.BAD_DIRECTION);
		}

	}

	/**
	 * Returns a random direction
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return a random direction
	 */
	public static Direction getRandomDirection() {
		Random rand = new Random();

		return values()[rand.nextInt(values().length)];
	}

}
