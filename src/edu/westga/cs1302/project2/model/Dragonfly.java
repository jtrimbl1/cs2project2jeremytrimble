package edu.westga.cs1302.project2.model;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;

/**
 * The Dragonfly Class
 * 
 * @author jeremy.trimble
 * @version 10/22/2018
 */
public class Dragonfly extends WingedInsect {

	private static final double SCALE_FACTOR = 100;

	/**
	 * Creates a new Dragonfly
	 * 
	 * @precondition color != null
	 * @postcondition new Dragonfly is created
	 * 
	 * @param x
	 *            the x value of the location of the Dragonfly
	 * @param y
	 *            the y value of the location of the Dragonfly
	 * @param size
	 *            the value of the width and height of the Dragonfly
	 * @param speed
	 *            the speed of the Dragonfly
	 * 
	 */
	public Dragonfly(double x, double y, double size, int speed) {
		super(x, y, size, size, speed);
	}

	private void drawBody(GraphicsContext gc) {
		gc.setFill(Color.BLACK);
		gc.setStroke(Color.BLACK);
		gc.fillOval(20, 45, 60, 5);
		if (this.getDirection() == Direction.LEFT) {
			gc.strokeArc(10, 32, 20, 30, 0, 80, ArcType.OPEN);
		} else {
			gc.strokeArc(75, 32, 20, 30, 180, -80, ArcType.OPEN);
		}
	}

	private void drawWings(GraphicsContext gc) {
		gc.setFill(Color.GRAY);
		gc.setStroke(Color.GRAY);
		gc.fillOval(35, 0, 10, 50);
		gc.fillOval(50, 0, 10, 50);

	}

	@Override
	public void draw() {
		GraphicsContext gc = this.getGraphicsContext2D();
		double xScale = this.getWidth() / SCALE_FACTOR;
		double yScale = this.getHeight() / SCALE_FACTOR;
		gc.scale(xScale, yScale);
		this.drawWings(gc);
		this.drawBody(gc);
	}

	@Override
	public String toString() {
		return "Dragonfly width=" + this.getWidth() + " speed = " + this.getSpeed();
	}

}
